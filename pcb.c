#include "pcb.h"
#include "listx.h"
#include "pandos_const.h"
#include "pandos_types.h"
#include <../listx.h>

static struct list_head* pcbFree_h;
static pcb_t pcbFree_table[MAXPROC];


void initPcbs(){ 
  for(int k = 0;  k < MAXPROC; k++){
    list_add(&pcbFree_table[k].p_list,pcbFree_h);    
  }    
}

void freePcb(pcb_t * p){ 
  if(p != NULL){
    list_add(&p->p_list,pcbFree_h);
  }
}

pcb_t *allocPcb(){
    if(list_empty(pcbFree_h)) return NULL; //se pcbFree_h è vuota, restituisce NULL.
    pcb_PTR toRet = pcbFree_h;
    pcbFree_h = pcbFree_h->p_next; //l'elemento viene rimosso in testa dalla lista pcbFree_h
    return setNull(toRet); //viene ritornato con tutti i campi settati a NULL
}

pcb_t *setNull(pcb_PTR tmp){
    if(tmp!=NULL){
        tmp->p_next = NULL;
        tmp->p_prev = NULL;
        tmp->p_prnt = NULL;
        tmp->p_child = NULL;
        tmp->p_next_sib = NULL;
        tmp->p_prev_sib = NULL;
	      tmp->p_semAdd = NULL;
        tmp->p_time = 0;
        tmp->p_supportStruct = NULL;
        return tmp;
    }
    else{
      return NULL;
    }
}

void mkEmptyProcQ(struct list_head *head) {
	INIT_LIST_HEAD(*head);
}

int emptyProcQ(struct list_head *head) {
	return list_empty(*head);
}

void insertProcQ(struct list_head *head, pcb_t *p) {
	list_add_tail(*head, *p);
}

pct_t headProcQ(struct list_head *head) {
	return head->next;
}

// Lista dei PCB
pcb_t *removeProcQ(struct list_head *head) {
	if (*head != NULL) {
		pcb_t temp = headProcQ(*head);
		list_del(headProcQ(*head));
		return *temp;
	} else {
		return NULL;
	}
}

pcb_t *outProcQ(struct list_head *head, pcb_t *p) {
	list_head *tmp = *head;
	
	while(list_is_last(*tmp, *head) != 0) {
		if(*p == *tmp) {
			list_del(*tmp);
			return *p;
		} else {
			tmp = *tmp->next;
		}
	}
	return NULL;
}

int emptyChild(pcb_t *p) {
	if(p->p_child != NULL) return 1;
	else return 0;
}

void insertChild(pcb_t *prnt, pcb_t *p) {
	insertProcQ(*prnt->p_child, *p);
}

pcb_t *removeChild(pcb_t *p) {
	if(emptyChild(*p) == 1) return NULL;
	return list_del(headProcQ(*p->p_child));
}

pcb_t *outChild(pcb_t *p) {
	if(*p->p_parent != NULL) {
		outProcQ(*p->p_sib, *p);
		return *p;
	} else { // p non ha un padre
		return NULL;
	}
}



































